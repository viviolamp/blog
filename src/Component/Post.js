import React from "react";
import {
  Card,
  CardText,
  CardImg,
  CardBody,
  CardTitle,
  Button,
} from "reactstrap";

export default function Post({ title, text, img }) {
  return (
    <Card className="postCard">
      <CardImg top width="100%" src={img} alt="Card image cap" />
      <CardBody>
        <CardTitle tag="h5">{title}</CardTitle>
        <CardText>{text}</CardText>
        <Button>Button</Button>
      </CardBody>
    </Card>
  );
}
