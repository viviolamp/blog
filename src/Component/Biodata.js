import React from "react";

export default function Biodata({ name, date, address, email }) {
  return (
    <div className="form">
      <form>
        <table>
          <tr>
            <td>Name</td>
            <td>: {name}</td>
          </tr>
          <tr>
            <td>Date</td>
            <td>: {date}</td>
          </tr>
          <tr>
            <td>Address</td>
            <td>: {address}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td>: {email}</td>
          </tr>
        </table>
      </form>
    </div>
  );
}
