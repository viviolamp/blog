import React from "react";
import { Col, Row } from "reactstrap";
import Post from "../Component/Post";
import image1 from "../Images/ijo.jpg";
import image2 from "../Images/merah.jpg";
import image3 from "../Images/tanah.jpg";

export default function Blog() {
  const posts = [
    {
      title: "Kacang Ijo",
      text:
        "Kacang hijau mengandung berbagai nutrisi dan vitamin yang menunjang kesehatan.",
      img: image1,
    },
    {
      title: "Kacang Merah",
      text:
        "Kacang merah mengandung berbagai nutrisi dan vitamin yang menunjang kesehatan.",
      img: image2,
    },
    {
      title: "Kacang Tanah",
      text:
        "Kacang tanah mengandung berbagai nutrisi dan vitamin yang menunjang kesehatan.",
      img: image3,
    },
  ];
  return (
    <div>
      <h3 className="kacang">Kacang-Kacangan</h3>
      <Row>
        {posts.map((post) => (
          <Col md="3">
            <Post title={post.title} text={post.text} img={post.img} />
          </Col>
        ))}
      </Row>
    </div>
  );
}
