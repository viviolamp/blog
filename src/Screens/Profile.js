import React from "react";
import Biodata from "../Component/Biodata";

export default function Profile() {
  return (
    <div className="formBig">
      <h4>PROFILE</h4>
      <Biodata
        name="Viviola Maya Putri"
        date="15 January 2002"
        email="viviolaputri15@gmail.com"
        address="Madiun, Indonesia"
      />
    </div>
  );
}
