import React, { Component } from "react";
import { Button, Form, FormGroup, Input } from "reactstrap";

export default class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
    };
  }

  clickHandler = (event) => {
    event.preventDefault();
    alert("Pesan kamu sudah ceterkirim");
  };

  render() {
    const { message } = this.state;
    return (
      <div className="formBig">
        <h4>Contact Us</h4>
        <Form inline onSubmit={this.clickHandler}>
          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Input
              onChange={(event) => {
                this.setState({ message: event.target.value });
              }}
              type="text"
              value={message}
            />
          </FormGroup>
          <Button>Submit</Button>
        </Form>
      </div>
    );
  }
}
