import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Home from "./Screens/Home";
import Profile from "./Screens/Profile";
import ContactUs from "./Screens/ContactUs";
import Blog from "./Screens/Blog";

function App() {
  return (
    <Router>
      <div className="navbar">
        <div className="navigator">
          <Link to="/">
            <h6>Home</h6>{" "}
          </Link>
          <Link to="/profile">
            <h6>Profile</h6>
          </Link>
          <Link to="/contact-us">
            <h6>Contact Us</h6>
          </Link>
          <Link to="/blog">
            <h6>Blog</h6>
          </Link>
        </div>
      </div>
      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={Profile} path="/profile" />
        <Route component={ContactUs} path="/contact-us" />
        <Route component={Blog} path="/blog" />
      </Switch>
    </Router>
  );
}

export default App;
